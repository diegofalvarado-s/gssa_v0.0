# Geographic Spectrum of Shared Alleles

This README file documents how to use the scripts in this repository to calculate the **Geographic Spectrum of Shared Alleles** (**GSSA**), a vector summary statistic introduced by Alvarado-Serrano and Hickerson's (*in review*) to uncover the spatial dynamics of range expansions from genetic data (set of biallelic SNPs). Additionally, it documents the steps necessary to replicate the simulations described in the paper.

______
## Overview
This repository contains two separate folders. The first folder, named **"GSSA_calculation"**" includes all the scripts necessary for the estimation of the GSSA from patterns of shared minor alleles between sampled localities.

The second folder, named **"GSSA_simulations"**, includes the simulation pipeline used in the original description of the GSSA (Alvarado-Serrano & Hickerson in review). This pipeline uses *fastsimcoal2* (Excoffier *et al*. 2013) to simulate patterns of genetic diversity under realistic range expansion scenarios, and summarize them into *multiSFS* vectors using *∂a∂i* (Gutenkunst *et al*. 2009). The pipeline then deploys the GSSA statistic along Peter and Slatkin's (2013) directionality index on these simulations to summarize the spatial dynamics of the expansion scenarios. 

______
## Disclaimer
Included in this repository are executables to proprietary software (most of which needed only if recreating Alvarado-Serrano & Hickerson's *in review* simulations; the GSSA calculation only requires *∂a∂i*). Users of the simulation pipeline should acknowledge the original software citations. Also, the simulation pipeline uses Peter and Slatkin's (2013) scripts, generously provided by B.M. Peter, and users of the simulation pipeline should also acknowledge these latter authors if inferences based on their method (implemented for comparison) are used.

______
## Dependencies
- [Arlequin v3.5.2.2](http://cmpg.unibe.ch/software/arlequin35/)
- [∂a∂i](https://bitbucket.org/gutenkunstlab/dadi)
- [Directionality index](www.bpeter.org)
- [fastsimcoal2](http://cmpg.unibe.ch/software/fastsimcoal2/)
- [Python v2.7](https://www.python.org/)
- [R v3.3.1 or later]()

______
## Set up
### GSSA_calculation
This directory contains the scripts necessary to **calculate the GSSA** from a *vcf* file and to summarize its elements--which describe the pattern of shared minor alleles between sampled localities, using Harpending's (1994) raggedness index. It also contains the scripts necessary to use the information captured from this summary statistic vector to estimate the spatial dynamics of a plausible range expansion. All steps could be run together with the *GSSA_calculation.sh* wrapper script included. For these calculations to work, in addition to a *vcf* file listing the set of sampled SNPs, a second input file listing either the coordinates of sampled localities (file suffix *.coord*) or the effective pairwise distances  between them (file suffix *.dist*) is needed. Examples of all these files are availble in the *example_files* folder. For detailed description of all steps involved check out the (python notebook)[https://mybinder.org/v2/gh/ftempo/GSSA/master] (also available in the **"notebook folder"**). For details on individual scripts, you can run any script using argument "*-h*". Note that the *∂a∂i* python module should be installed for the conversion of the vcf file into a *multisfs* file to run.

The **example_files** subdirectory contains examples of the two input files necessary to estimate the GSSA statistic:

- **Sampled_locs.coord**: file listing the geographic coordinates of sampled localities (one line per sampled locality).

- **Sampled_locs.dist**: comma-separated file listing the effective distances among all sampled localities (provided as a full pairwise matrix)

- **Sampled_SNPs.vcf**: example vcf file containing a set of 5000 biallelic SNPs.

- **multiSFS_Sampled_SNPs.sfs**: multiple-SFS file obtained from the example vcf file (using the *multiSFS_creator.py* script included in the pipeline)

To estimate the GSSA and infer the plausible range expansion spatial dynamics:

	#GSSA inference:
		#./GSSA_calculation.sh [input vcf file path] [samples coordinates (or distances) file path] [whether to produce plots: TRUE/FALSE]
	
		./GSSA_calculation.sh example_files/Sampled_SNPs.vcf example_files/Sampled_locs.coord TRUE	#using coordinates as input
		
		#OR
		
		./GSSA_calculation.sh example_files/Sampled_SNPs.vcf example_files/Sampled_locs.dist TRUE	#using distances as input

This script would produce 2 output files:

- ***&ast;_raggedness_index.tsv*** : tab-separated file reporting the Raggedness Index calculated from the GSSA of each sampled locality.

- ***&ast;_GSSA_results.tsv*** : tab-separated file listing the location (with geographic coordinates if provided) identified as the expansion origin (if the localities sampled included the geographic expansion source area) or the first locality, among those sampled, colonized as the expansion proceeds (if the sampled localities do not include the expansion source area).

______
### GSSA_simulations
This directory contains the input files necessary to **run the range expansion simulations** implemented in Alvarado-Serrano and Hickerson (in review). The simulation pipeline should be started by calling *simulation_runner.sh*, which first generates customized *fastsimcoal2* input files for sets of simulations starting from different expansion sources, and then uses those files to run spatially implicit simulations under specific parameter settings. The number of simulation replicates to run and the number of SNPs to simulate are determined by the first and second arguments of the *simulation_runner.sh* script. All other scripts and input files provided should be in the same folder as the *simulation_runner.sh* script, including the *fastsimcoal2* (*fsc252*) executable, and *∂a∂i* python module should be installed for the simulations to run.

Briefly, the simulation pipeline proceeds by performing the tasks described below (the scripts involved in each step are mentioned in square brackets; for detailed description of these scripts' arguments and purpose, run each script using argument "*-h*"):

1. generate scenario-specific *&ast;.tpl* files for *fastsimcoal2* from provided colonization history files for each simulated source (*colonizationT_S&ast;.txt*). These files detail the time at which each deme in the simulated world is colonized and where did the colonizers originate [*tpl.creator.py*].

2. customize provided generic *fastsimcoal2* estimation file  (*100Pop_SeqExp2D-DEFAULT.est*) with specific parameter values for each simulation set. Note this step is not necessary for the Isolation-By-Distance scenario simulation as it uses an already customized *.tpl* file called *100Pop_SeqExp2D_IBD.est* [*customize_est_file.py*].

3. run range expansion simulations using *fastsimcoal2*. If a large number of simulations is requested, they can be run in batches to avoid the piling-up of temporary intermediate files [*GSSA_simulator.py*].

4. generate input file for Psi calculation from *Arlequin* (*&ast;.arp*) files generated as output by simulations [*extract_SNPs_arlfile.py*, *python snp_to_snapp.py*]

5. generate input file for *∂a∂i*, from *Arlequin* (*&ast;.arp*) files generated as output by simulations, to estimate the multiSFS file required for the GSSA calculation [*extract_SNPs_arlfile.py*].

6. estimation of multiSFS file from input generated in previous step [*dadi_SFS_creator.py*, *GSSA_simulator.py*].

7. output a file (*&ast;.param*) summarizing parameter values used in each set of simulations [*GSSA_simulator.py*]

8. erase all unnecessary temporary files [*GSSA_simulator.py*]

To run the entire simulation pipeline, use:

	#Run simulations
		#./simulation_runner.sh [number of simulation replicates] [numer of SNPs] [number of cores] [number of simulation batches]
	
		./simulation_runner.sh 1000 5000 0 10

Also included in this directory are the scripts needed to calculate the GSSA statistic (Alvarado-Serrano and Hickerson *in review*) and, for comparison, Peter and Slatkin's (2013) directionality index (Psi) from the output of the simulations. Both statistics are then used to estimate the spatial dynamics of the simulated expansions. Note that the directionality index's calculation uses scripts generously provided by BM Peter, which were only slighly modified to be integrated into the simulation pipeline (all changes made to Peter's scripts are clearly marked in the corresponding scripts; see below).

To uncover the spatial dynamics of the simulated range expansions, using the new GSSA and Psi statistics, use (note that because the simulation files are compressed after calculating the GSSA, it is expected that Psi's calculation, if it is of interest, is run first):

	#Calculate Peter and Slatkin's directionality index
		#./Psi_calculation.sh [sample coordinates file] [numer of sampled localities]
		
		./Psi_calculation.sh Sampled_locs.coord 10
	
	
	
	#Calculate the spatial-SFS
		#./GSSA_calculation_sim.sh [coordinates file] [whether to plot]
		
		./GSSA_calculation_sim.sh Sampled_locs.coord FALSE

Running these calculations would produce the following output files:

- **GSSA_simulations.tar.gz** : tar-gzipped containing all the simulation files compressed, including the *&ast;.snapp* and *&ast;.sfs* files necessary to estimate Psi and GSSA statistics, respectively.

- **Psi_index_combined.tsv** : tab-separated file reporting the directionality index results for each simulation run, which include the coordinates of the inferred range expansion source.

- **Raggedness_index_combined.tsv** : tab-separated file reporting the Raggedness Index calculated on the GSSA of each sampled locality and simulation run (with the number of rows per simulation corresponding to the number of sampled localities).

- ***GSSA_combined.tsv*** : tab-separated file listing the location (with geographic coordinates) identified as the expansion origin (if the localities sampled included the geographic expansion source area) or the first locality, among those sampled, colonized as the expansion proceeds (if the sampled localities do not include the expansion source area) for each simulation run. This file would contain one line per simulation replicate.

______
## References
- Alvarado-Serrano, D.F. and Hickerson, M.J. *In review*. Detecting spatial origins of range expansions with geo-referenced genome-wide SNP data and the geographic spectrum of shared alleles. 

- Harpending, H.C. 1994. Signature of ancient population growth in a low-resolution mitochondrial DNA mismatch distribution. Human Biology 66: 591-600.

- Peter, B.M. and Slatkin, M.. 2013. Detecting range expansions from genetic data. Evolution 67: 3274-3289.

---
## Contributors
- [Diego F. Alvarado-Serrano](http://alvarado-s.weebly.com) (E-mail: dalvarad[at]umich.edu)

- Michael H. Hickerson