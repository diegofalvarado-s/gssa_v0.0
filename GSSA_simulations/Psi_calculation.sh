#!/bin/sh

#wrapper script to estimate Peter and Slatkin's (2013) directionality index. Note this script,
#and the scripts it relies on are specifically tailored to the set of simulations run in
#Alvarado-Serrano & Hickerson (in review).
#Required files:
#	- snapp [*.snapp]: input file to estimate Psi (as required re_analysis_DFA2.r script)
#	- Psi's script [re_analysis_DFA2.r]: R script to calculate Psi (provided by BM Peter)
#	- Psi's functions [re_functions_DFA2.r]: file with R functions necessary for
#											 re_analysis_DFA2.r to run
#	- replicates condenser script [Psi_outcondenser.py]: script to condense the Psi results
#														 into a single output file
#	- samples coordinates [Sampled_locs.coord]: tab-separated file providing the geographic
#												coordinates of all sampled localities
#Usage: ./Psi.sh [sample coordinates file] [numer of sampled localities]
#Diego F. Alvarado-S., Jan 26, 2018

Infile=$1
Nloc=$2

if [ "$1" = "-h" ]
then
	echo
	echo 'Usage: ./Psi_calculation.sh (1) (2)'
    echo 'Purpose: calculate Psi statistic and estimate range expansion origin using it'
    echo 'Arguments:'
    echo '\t(1): sampled localities coordinates input file'
    echo '\t(2): number of sampled localities'
	echo
else
	for f in $(ls -l | grep "^d" | awk '{print $9}' | grep "100Pop_SeqExp")
	do
		python Psi_runner.py -f $f -c $1 -n $2 > temp_Psi_screen_print.txt
		rm temp_Psi_screen_print.txt
	done
python Psi_outcondenser.py
fi
