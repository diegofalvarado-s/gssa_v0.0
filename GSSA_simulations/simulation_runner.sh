#!/bin/sh

#wrapper script to customize generic tpl input files for specific parameter values and then run fastsimcoal2
#simulations using the modified files. Note this script, and all the associated input files it relies on (see
#below), are specifically tailored to the set of simulations run in Alvarado-Serrano & Hickerson (in review)
#and should be modified if to be used to run a different set of simulations.
#Required files:
#	- fastsimcoal2 [fsc252]: executable fastsimcoal2 file
#	- colonization history [colonizationT_S*.txt]: tab-separated files reporting the simulation time at which 
#												   each deme is colonized and whether it should be sampled
#								  				   (i.e., genetic data generated for) 
#	- migration matrix [mig_matrix.txt]: tab-separated file listing the set of migration rates across demes
#										 at each time step (see fastsimcoal2 manual for details)
#	- generic fastsimcoal2 estimation [*.est]: file defining the parameter values to be used in the
#											   simulations (see fastsimcoal2 manual for details)
#Usage: ./simulation_runner.sh [number of simulation replicates] [numer of SNPs] [number of cores]
#		[number of simulation batches]
#Diego F. Alvarado-S., Jan 26, 2018

NRep=$1
Nsnp=$2
Ncor=$3
Nbat=$4

if [ "$1" = "-h" ]
then
	echo
	echo 'Usage: ./simulation_runner.sh (1) (2) (3) (4)'
    echo 'Purpose: run range expansion fastsimcoal2 simulations'
    echo 'Arguments:'
    echo '\t(1): number of simulation replicates to run'
    echo '\t(2): number of SNPs to simulate'
	echo '\t(3): number of cores tor un the simulation on [use 0 to let the script choose the optimal value]'
    echo '\t(4): number of batches to split the simulations into (to avoid pile-up of temporary files)'
	echo
else
	#generating fastsimcoal2 input files
	for i in 0 44 50 68
	do
		python tpl_creator.py -t colonizationT_S$i.txt -m mig_matrix.txt -n $2 -o 100Pop_SeqExp2D-S$i.tpl > summary_simulated_expansion_S$i.txt
	done
	rm ./summary_simulated_expansion_*.txt
	
	#run multiple expansion simulations in fastsimcoal2
	for p in AFT FPR MI0 TGR
	do
		for j in 1 2 3
		do
			v="$(python customize_est_file.py -e 100Pop_SeqExp2D-DEFAULT.est -p $p -s $j)"
			for i in 0 44 50 68
			do
				cp 100Pop_SeqExp2D_$p$v.est 100Pop_SeqExp2D_$p$v-S$i.est
				cp 100Pop_SeqExp2D-S$i.tpl 100Pop_SeqExp2D_$p$v-S$i.tpl
				python GSSA_simulator.py -p 100Pop_SeqExp2D_$p$v-S$i -n $NRep -s $Nsnp -c $3 -r $4
				rm 100Pop_SeqExp2D_$p$v-S$i.est 100Pop_SeqExp2D_$p$v-S$i.tpl seed.txt
			done
		rm 100Pop_SeqExp2D_$p$v.est
		done
	done
	rm *.tpl
fi
