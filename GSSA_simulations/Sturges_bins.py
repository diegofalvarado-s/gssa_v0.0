#!/usr/bin/env python

'''
simple script to calculate histogram bins using Sturge's equation. It takes a list of numeric values as input and produces a list of Sturge's bins as output
Diego F. Alvarado-S., April 23, 2015
'''

import math, numpy as np

def sturges(vals):
    N = len(vals)
    nbins = math.ceil(math.log(N,2) + 1)
    bins = np.linspace(min(vals), max(vals), nbins)
    return list(bins)