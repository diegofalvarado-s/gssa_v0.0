#!/usr/bin/env python

'''
script to modify a generic fastsimcoal2 .est file with specific parameter values.
Note this script is specific to the simulations implemented in Alvarado-Serrano and
Hickerson (in review) and need to be modified if to be used for a different project.
Usage: python customize_est_file.py -e [generic .est file] -p [focal parameter] -s [parameter set]
Diego F. Alvarado-S., Oct-31-2017
'''

from optparse import OptionParser

usage = "Usage: %prog -e -p -s"
description = "Purpose: to modify a general .est file with specific parameter values"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-e", dest="estFile", help="path to generic .est file which is to be customized")
parser.add_option("-p", dest="Param", help="abbreviation of parameter which value is to be modified in .est file")
parser.add_option("-s", dest="Set", help="parameter set value to use")
(options,args) = parser.parse_args() 

infilename = options.estFile
param = options.Param
value = int(options.Set)

#definition of parameter values to simulate under
params = {'AFT': ['0', '0.5', '1'],
		  'TGR': ['0.01', '0.1', '0.05'],
		  'FPR': ['0.002', '0.01', '0.1'],
		  'MI0': ['0', '0.001', '0.01']}
outfilename = '%s_%s%s.est' %(infilename.split('.')[0].replace('-DEFAULT',''), param, params.get(param)[(value-1)].replace('.',''))

#generating customized .est file
with open(outfilename, 'w') as outfile:
	with open(infilename, 'r') as infile:
		for line in infile:
			start = '0\t%s' %param
			if line.startswith(start):
				row = line.strip().split()
				print >> outfile, '0\t%s\tunif\t%s\t%s\thide' %(param, params.get(param)[(value-1)], params.get(param)[(value-1)])
			else:
				print >> outfile, line.strip()

#print to screen parameter value used to customize .est file
print params.get(param)[(value-1)].replace('.','')
