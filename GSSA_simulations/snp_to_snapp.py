#!/usr/bin/env python

'''
script to turn a snp file (created from arlequin output using extract_SNPs_arlfile.py)
into a snapp file
Usage: python snp_to_snapp.py -f [infile]
Diego F. Alvarado-S., Dec 12, 2014
'''

import sys, numpy as np
from optparse import OptionParser

usage = "Usage: %prog -f"
description = "Purpose: to convert a snp file into a snapp file"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-f", dest="InFile", help="path to .snp file to be converted")
(options,args) = parser.parse_args() 

infilename = options.InFile
outfilename = infilename.replace('.snp', '.snapp')

with open(outfilename, 'w') as outfile:
    with open(infilename ,'r') as infile:
        n = 0
        for line in infile:
            n += 1
            row = line.strip().split()
            if n%2 == 1:
                indname = row[0]
                row1 = [int(x) for x in row[1:]]
            elif n%2 == 0:
                data = np.sum(np.vstack((row1, [int(x) for x in row[1:]])), axis=0)
                data = [str(y) for y in data.tolist()]
                data = ['?' if int(z) not in range(3) else z for z in data]
                print >> outfile, 'P%s,%s' %(indname.split('_')[0], ','.join(data))
                del indname
                del row1
                del data

