#!/usr/bin/env python

'''
script to estimate Peter & Slatkin's directionality index (Psi) from multiple snapp files
Usage: python Psi_runner.py -f [input folder] -c [samples coordinate file] -n [number points for estimation]
Diego F. Alvarado-S., May 14, 2015
'''

import os, subprocess, shlex
from optparse import OptionParser

usage = "Usage: %prog [-f] -c -n"
description = "Purpose: estimate Peter & Slatkin's directionality index from multiple snapp files"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-f", dest="Folder", default='./', help="path to the folder with the snapp files")
parser.add_option("-c", dest="coordfile", help="path to the locality coordinates file")
parser.add_option("-n", dest="points", help="number of points for origin estimation")
(options,args) = parser.parse_args() 

fold = options.Folder
if not fold.endswith('/'):
    fold = fold + '/'
coords = options.coordfile
npoints = options.points

outfilename = '%s_Psi_results.tsv' %fold[:(len(fold)-1)]
with open(outfilename,'w') as outfile:
    n = 0
    for F in os.listdir(fold):
        if F.endswith('.snapp'):
            n +=1 
            
            #running Psi calculation
            pathF = '%s/%s' %(fold, F)
            cmd = 'Rscript re_analysis_DFA2.r %s %s %s False False' %(pathF, coords, npoints)     #this assumes that because of the multiple runs, no plots or additional files are needed. If not replace the last 2 "False" (1st for plots; 2nd for additional analyses)
            job = subprocess.call(shlex.split(cmd))
            
            #storing Psi results
            result_file = 'table_out_%s.txt' %F
            replacement = 'table_out_%s_' %fold.replace('./','')
            cutname = result_file.replace(replacement,'').replace('.snapp.txt', '')
            
            #reading in results ans storing them in new files
            if os.path.isfile(result_file):
                with open(result_file, 'r') as infile:
                    m = 0
                    for line in infile:
                        m += 1
                        if n == 1 and m == 1:
                            print >> outfile, 'Rep\t%s' %('\t'.join(line.strip().split()))
                        elif m == 2:
                            print >> outfile, '%s-%s\t%s' %(cutname.split('_')[0], cutname.split('-')[-1], '\t'.join(line.strip().split()))
            
            #printing NA for instances where Psi calculation failed
            else:
                print >> outfile, '%s-%s\tRegion1\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA' %(cutname.split('_')[0], cutname.split('-')[-1])
            
            #removing temporary files
            for temp in os.listdir(os.getcwd()):
                if F in temp:
                    os.remove(temp)
            if os.path.isfile('tmp2'):
                os.remove('tmp2')
