#!/bin/sh

#wrapper script to estimate the geographic spectrum of shared alleles (GSSA) summary statistic
#vector and use it to estimate the spatial dynamics of a potential range expansion on a set
#of simulations. Note this script, and the scripts it relies on are specifically tailored to
#the set of simulations run in Alvarado-Serrano & Hickerson (in review).
#Usage: ./GSSA_calculation_sim.sh [coordinates file] [whether to plot]
#Diego F. Alvarado-S., Jan 26, 2018

COORDS=$1
PLOT=$2

if [ "$1" = "-h" ]
then
	echo
	echo 'Usage: ./GSSA_calculation_sim.sh (1) (2) (3)'
    echo 'Purpose: calculate GSSA statistic and estimate range expansion spatial dynamics using it on a set of simulations'
    echo 'Arguments:'
    echo '\t(1): multiSFS input file'
    echo '\t(2): sampled localities coordinates input file'
	echo '\t(3): whether to plot the estimated GSSA vectors [TRUE/FALSE]'
	echo
else
	#calculate geographic-distance and geo-genetic histograms as described in Alvarado-Serrano and Hickerson in review
	#calculate uncorrected spatialSFS (a.k.a. geo-histogram' Alvarado-Serrano and Hickerson in review)
	for p in AFT FPR MI0 TGR
	do
		for j in 1 2 3
		do
			v="$(python customize_est_file.py -e 100Pop_SeqExp2D-DEFAULT.est -p $p -s $j)"
			rm 100Pop_SeqExp2D_$p$v.est
			for i in 0 44 50 68
			do
				python histograms_creator_sim.py -f 100Pop_SeqExp2D_$p$v-S$i --coord $1
			done
		done
	done
	
	#identify spatial dynamics of possible range expansion
	for f in $(ls ./*.histo)
	do
		Rscript origin_finder_sim.R $f Sampled_locs.coord $2
	done
	rm ./*.histo

	#condense simulation results and compress (tar.gz) simulation files
	python GSSA_outcondenser.py
fi
