#!/usr/bin/env python

'''
simple script to condense output files from multiple GSSA calculations on simulated data.
Note that this script is specific for the simulations implented in Alvarado-Serrano and
Hickerson pipeline and it would have to be customized for other projects.
Usage: python GSSA_outcondenser.py -d [output directory]
Diego F. Alvarado, Jan-20-2018
'''

import os, tarfile, shutil
from optparse import OptionParser
#tar = tarfile.open("GSSA_simulations.tar.gz", "w:gz")	#definining path to tar.gz file to store all simulation files

usage = "Usage: %prog"
description = "Purpose: to condense output files from multiple GSSA calculations on simulated data"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-d", dest="OutDir", default='./', help="path to output folder where .GSSA files are stored [default current directory]")
(options,args) = parser.parse_args()

outdir = options.OutDir
if not outdir.endswith('/'):
	outdir = outdir + '/'

#defining output files
raggout = '%sRaggedness_index_combined.tsv' %outdir
GSSAout = '%sGSSA_combined.tsv' %outdir

#reading and ptocessing input files
m = 0
with tarfile.open("GSSA_simulations.tar.gz", "w:gz") as tar:	#definining path to tar.gz file to store all simulation files
	with open(raggout, 'w') as raggfile:
		with open(GSSAout, 'w') as spSFSfile:
			for F in os.listdir(outdir):
				
				#combining individual raggedness index files
				if F.endswith('_raggedness_index.tsv'):
					m += 1
					
					#defining prefixes to identify individual simulation batches
					parcomb = F.split('_')[2]
					param = parcomb.split('-')[0][:3]
					parval = parcomb.split('-')[0][3:]
					source = parcomb.split('-')[1]
					File = '%s%s' %(outdir, F)
					with open(File, 'r') as infile:
						n = 0
						for line in infile:
							n += 1
							row = line.strip()
							if m == 1:
								if n == 1:
									print >> raggfile, 'Parameter\tValue\tSource\t%s' %row
							else:
								if n > 1:
									print >> raggfile, '%s\t%s\t%s\t%s' %(param, parval, source, row)
					
					#removing individual raggedness files after being processed
					os.remove(File)
					
					#combining individual GSSA results files
					File2 = File.replace('_raggedness_index.tsv', '_GSSA_results.tsv')
					with open(File2, 'r') as infile:
						n = 0
						for line in infile:
							n += 1
							row = line.strip()
							if m == 1:
								if n == 1:
									print >> spSFSfile, 'Parameter\tValue\tSource\t%s' %row
							else:
								if n > 1:
									print >> spSFSfile, '%s\t%s\t%s\t%s' %(param, parval, source, row)
					
					#removing individual GSSA results files after being processed
					os.remove(File2)
				
				#condensing all simulation files
				elif os.path.isdir(F):
					path = '%s%s' %(outdir, F)
					tar.add(path)
					shutil.rmtree(path)
