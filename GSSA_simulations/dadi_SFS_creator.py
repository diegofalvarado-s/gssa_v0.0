#!/usr/bin/env python

'''
script to generate SFS and/or jointSFS using dadi module and a .dadi input file
Usage: python dadi_SFS_creator.py -F [input file] --multi [whether multiSFS is created] --polar [whether polarized or non-polarized SFS is created] --doPlot [whether plots are created]
Diego F. Alvarado-S., Nov 13, 2014
'''

import os, copy, dadi, numpy as np
from optparse import OptionParser
from itertools import combinations
from matplotlib.backends.backend_pdf import PdfPages

usage = "Usage: %prog [options] arg"
description = "Purpose: to generate SFS using dadi"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-F", dest="Filename", help="name of the snapp input file being converted")
parser.add_option("--multi", dest="multiSFS", default=False, help="whether multiSFS should be created [True/False*]")
parser.add_option("--polar", dest="polarize", default=False, help="whether folded (T) or unfolded (F) SFS should be created [True/False*]")
parser.add_option("--plot", dest="Plot", default=False, help="whether to plot the estimated SFS [True/False*]")
(options,args) = parser.parse_args()

infilename = options.Filename
prefix = infilename.split('/')[-1].split('.')[0]
doMulti = options.multiSFS
doPolar = options.polarize
if doPolar == 'True':
    doPolar = True
doPlot = options.Plot
if doPlot == 'True':
    doPlot = True

#determining populations and their sample sizes
n = 0
with open(infilename, 'r') as infile:
    sampleN = {}
    for line in infile:
        n += 1
        
        #creating dictionaries to store the SNP data counts
        if n == 1:
            header = line.strip().split()
            All1Pos = header.index('Allele1')
            All2Pos = header.index('Allele2')
            popnames = header[(All1Pos+1):All2Pos]
            sampleNtemp = {}
            for pop in popnames:
                sampleNtemp.setdefault(pop,[])
                sampleN.setdefault(pop,[])
            Counts1head = header[(All1Pos+1):All2Pos]
            Counts2head = header[(All2Pos+1):(All2Pos + len(popnames) + 1)]
        
        #populating dictionaries with SNP counts
        else:
            row = line.strip().split()
            Counts1 = row[(All1Pos+1):All2Pos]
            Counts2 = row[(All2Pos+1):(All2Pos + len(popnames) + 1)]
            for pop in popnames:
                sampleNtemp[pop].append(int(Counts1[Counts1head.index(pop)]))
                sampleNtemp[pop].append(int(Counts2[Counts2head.index(pop)]))
            for pop in popnames:
                sampleN[pop].append(sum(sampleNtemp.get(pop)))
                sampleNtemp[pop] = []    #this is just to have the original dictionary reset

#creating dadi dictionary with the information for each locality/population
dadiIN = dadi.Misc.make_data_dict(infilename)
dtype = [('locname', 'S10'), ('Nsamples', int), ('order', int)]
values = []

#deterimning the minimum number of sampled individuals per locality/population to downproject estimation
for loc in popnames:
    values.append((loc, min(sampleN.get(loc)), 0))
locOrder = np.array(values, dtype=dtype)       #creates a structured array with all the info needed included minimum sample size for each loc
if np.std(locOrder['Nsamples']) > 0.0:
    locOrder = np.sort(locOrder, order=['Nsamples', 'locname'])

#calculating jointSFS
if len(sampleN.keys())>1:
    #identifying all loc/pop pairs
    for i in xrange(len(locOrder)):
        locOrder[i][2] = len(locOrder) - i
    locPairs = list(combinations(list(locOrder[['locname'][0]]),2))

    #calculating and plotting estimated jointSFSs
    if doPlot:
        print 'doPlot set to TRUE:'
        pdfName = '%s_SFSs_biplot.pdf' %prefix
        plotOut = PdfPages(pdfName)
        from matplotlib import pyplot as plt
        for pair in locPairs:
            pairLoop = list(pair)
            dataP1 = list(locOrder[locOrder['locname']==pairLoop[0]][0])
            dataP2 = list(locOrder[locOrder['locname']==pairLoop[1]][0])
            pairN = [dataP1[1], dataP2[1]]
            name = '%s_jointDAF_%s_pop%d_%d.sfs' %(prefix, '-'.join(pairLoop), dataP1[2], dataP2[2])
            
            #estimating jointSFS
            SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = pairLoop, projections = pairN , polarized = False)     #returns the *folded* spectrum
            
            #if requested, output joint SFS
            if not doMulti:
                SFS.to_file(name)
            
            #saving plots into file
            SFSplot = dadi.Plotting.plot_single_2d_sfs(SFS , vmin = 1)
            plotOut.savefig(SFSplot)
            plt.close()     #this was needed to prevent the number of legend colorbar to be continuosly added in ech loop
        plotOut.close()
else:
    print '\n\t\tNumber of populations = 1, no joint SFS produced\n'
    
    #generating individual SFS and storing them
    name = '%s_singleAF.sfs' %(prefix)
    SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = [locOrder[0][0]], projections = [locOrder[0][1]] , polarized = doPolar)
    SFS.to_file(name)    
 
    
#if requested, generating multiSFS 
if doMulti:
    #generating multiSFS and storing it
    SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = locOrder['locname'], projections = locOrder['Nsamples'] , polarized = doPolar)
    SFS.to_file('multiSFS.sfs')