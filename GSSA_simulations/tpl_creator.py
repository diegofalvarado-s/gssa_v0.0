#!/usr/bin/env python

'''
script to create custom .tpl files based on custom migration and colonization history files, which,
respectively report the simulation time at which each deme is colonized and sampled (i.e., genetic
data generated for) and the set of migration rates across demes at each time step.
Usage: python tpl_creator.py -t [colonization file] -m [migration matrix] -n [number of SNPs] -o [output name]
Diego F. Alvarado-S., April 24, 2016
'''

import math, copy, subprocess, shlex, numpy as np
from optparse import OptionParser

usage = "usage: %prog -t -m [-n] -o"
description = "Purpose: to create custom .tpl files based on migration and colonization history files"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-t", dest="Coltimes", help="path to colonization times file")
parser.add_option("-m", dest="Migmat", help="path to file containing migration pattern")
parser.add_option("-n", dest="NSPNs", default='5000', help="number of SNPs to simulate")
parser.add_option("-o", dest="outpath", help="name of ouput tpl file")
(options,args) = parser.parse_args() 

coltimesname = options.Coltimes
migmatname = options.Migmat
Nsnps = options.NSPNs
outfilename = options.outpath

times = {}  #stores the times at whcih each deme is colonized as the expansion proceeds
samsites = []   #stores which demes should be sampled (i.e., generated genetic data for)

#reading in and storing information from colonization file
with open(coltimesname, 'r') as infile:
    Nsite = -1
    for line in infile:
        Nsite += 1
        if Nsite > 0:
            row = line.strip().split()
            times.setdefault(int(row[0]),[])
            times[int(row[0])].append(row[1])
            if row[2] == 'y':
                samsites.append(int(row[1].replace('S','')))

#generating customized .tpl file
with open(outfilename, 'w') as outfile:
    #SAMPLES
    print >> outfile, '//Number of population samples (demes)'
    print >> outfile, '%d' %Nsite
    
    #EFFECTIVE SIZES
    print >> outfile, '//Population effective sizes (number of genes)'
    for S in xrange(Nsite):
        print >> outfile, 'NeC'
    
    #SAMPLE SITES
    print >> outfile, '//Sample sizes'
    for S in xrange(Nsite):
        if S in samsites:
            print >> outfile, '2\t0'
        else:
            print >> outfile, '0\t0'
    
    #GROWTH RATES
    print >> outfile, '//Growth rates : negative growth implies population expansion'
    for S in xrange(Nsite):
        print >> outfile, '0'
    
    #MIGRATION MATRICES
    print >> outfile, '//Number of migration matrices : 0 implies no migration between demes'
    print >> outfile, '%d' %(max(times.keys())+1)
    
    migmat = np.zeros((Nsite, Nsite))
    with open(migmatname, 'r') as allmigs:
        m = -1
        for line in allmigs:
            m += 1
            row =  line.strip().split()
            for x in xrange(len(row)):
                if row[x] == 'MIG':
                    migmat[x,m] = 1.0  
    
    print >> outfile, '//Migration matrix 0'
    for R in xrange(np.shape(migmat)[0]):
        print >> outfile, '\t'.join([str(x) for x in migmat[R,]]).replace('1.0','MIG')
    
    n = 0
    uncolonized = []
    for t in reversed(xrange(1,max(times.keys())+1)):
        n += 1
        migmatT = copy.deepcopy(migmat)
        colsites = times.get(t)
        for site in colsites:
            uncolonized.append(site)
        for y in uncolonized:
            migmatT[int(y.replace('S','')),:] = [0]*Nsite
            migmatT[:,int(y.replace('S',''))] = [0]*Nsite
        print >> outfile, '//Migration matrix %d' %n
        for R in xrange(np.shape(migmatT)[0]):
            print >> outfile, '\t'.join([str(x) for x in migmatT[R,]]).replace('1.0','MIG')

    #EVENTS
    print >> outfile, '//historical event: time, source, sink, migrants, new size, new growth rate, migr. matrix'
    print >> outfile, 'XX\thistorical\tevent'
    currsites = range(Nsite)
    T = 0
    Nevent = 0
    for t in reversed(xrange(max(times.keys())+1)):
        T += 1
        for z in [int(x.replace('S','')) for x in times.get(t)]:
            currsites.remove(z)
        itersites = [int(x.replace('S','')) for x in times.get(t)]
        
        #deme expansion
        for s in itersites:
            time = '0'+str(T) if T <= 9 else str(T)
            print >> outfile, 'T%sa\t%d\t%d\t0.0\t1\tGRT\t%s' %(time,s,s,(T-1))
            Nevent += 1
            
        #coalescence
        for s in itersites:        
            sources = [i for i, x in enumerate(migmat[s,:].tolist()) if x == 1.0]
            sourcecopy = sources[:]
            for r in sourcecopy:
                if r not in currsites:
                    sources.remove(r)
            print 'Time', T, ':', s, '-->', sources
            if len(sources) > 2:  
                print 'WARNING: immigration rates for Deme %d needs to be corrected' %s
                
            W = 0
            for r in sources:
                W += 1
                mig = '0.5' if W == 1 else '1'
                mig = '1' if len(sources) == 1 else mig
                time = '0'+str(T) if T <= 9 else str(T)
                print >> outfile, 'T%sb\t%d\t%d\t%s\t1.0\t0\t%s' %(time,s,r,mig,T)
                Nevent += 1
                
    #GENETIC DATA
    print >> outfile, '//Number of independent loci [chromosome]'
    print >> outfile, '%s\t0' %Nsnps
    print >> outfile, '//Per chromosome: Number of linkage blocks'
    print >> outfile, '1'
    print >> outfile, '//per Block: data type, num loci, rec. rate and mut rate + optional parameters'
    #print >> outfile, 'SNP    1   0    0.001'
    print >> outfile, 'DNA 100 0.00000 2e-4 0.33'
    
cmd = "sed -i 's/XX\thistorical\tevent/%d\thistorical\tevent/g' %s" %(Nevent, outfilename)
job = subprocess.call(shlex.split(cmd))