#!/usr/bin/env python

'''
script to run a set of range expansion simulations using fastsimcoal2, remove intermediate
files, convert fastsimcoal2 output files into snp files, and use these snp files to
generate multiSFS files using dadi, and snapp files--to be used as input for the estimation
of Peter and Slatkin's (2013) directionality index (Psi). Note that to create the folded
spectrum, the minor allele is considered as ancestral for polarization. Also note that
currently the script assumes only one diploid individual sampled per location (this could
be easily modified, but at the expense of memory issues; so the easiest solution is to
down-project if necessary in dadi to 1 diploid individual).
Usage: python spatialSFS_simulator.py -p [input files prefix] -n [number of simulation replicates] -s [number of SNPS to simulate] -c [number of cores to use] --DirIndex [whether to calculate generate Psi]
Diego F. Alvarado-S., April 14, 2015
'''

import os, sys, copy, subprocess, shlex, numpy as np
from optparse import OptionParser

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-p",dest="prefix", help="name of fastsimcoal input file prefixes")
parser.add_option("-n",dest="Ndraws", help="number of parameter set draws to take")
parser.add_option("-s",dest="NSPNs", default='5000', help="number of SNPs to simulate")
parser.add_option("-c",dest="Ncores", default="0", help="number of cores to allow fsc to run on") #0 means let fsc c
parser.add_option("-r",dest="Nruns", default="100", help="max number of fsc simulations to run at once")   #to avoid a pile up of the files
parser.add_option("--DirIndex",dest="doDir", default=True, help='whether to create otput to estimate Psi [True/False]')
(options,args) = parser.parse_args() 

pre = options.prefix
dra = options.Ndraws
Nsnps = options.NSPNs
cores = options.Ncores
outdir = './%s' %(pre)
maxruns = int(options.Nruns)
if options.doDir:
    doDirIndex = True
else:
    doDirIndex = False
paramfilename = './%s.params' %(pre)    #path to output file storing the parameter values used in each  simulation ets

#running fastsimcoal and generating the appropiate output
with open(paramfilename, 'w') as paramfile:
    if int(dra) <= maxruns:
        #running fastsimcoal2 simulations
        cmdA = './fsc252 -t %s.tpl -n %s -e %s.est -E 1 -q -s %s -c %s' %(pre, dra, pre, Nsnps, cores)
        job = subprocess.call(shlex.split(cmdA))
        
        #stopping simulations if fastsimcoal2 fails
        if job != 0:
            sys.exit('\nERROR: fastisimcoal run with error, pipeline stopped!\n')
        
        #processing fastsimcoal2 output files
        for F in os.listdir(outdir):
            pathF = '%s/%s' %(outdir, F)
            
            #deleting unnecessary files
            if F.endswith('.par') or F.endswith('.arb') or F.endswith('.simparam'):
                os.remove(pathF)
                
            #converting output arlequin (.arp) files into .snp files
            elif F.endswith('.arp'):
                cmdB = 'python ./extract_SNPs_arlfile.py -d %s' %(outdir)
                job = subprocess.call(shlex.split(cmdB))
                #removing .arp file after being processed
                os.remove(pathF)
                
    #this allows to run the simulation and process the output in batches instead of all at once
    #and thus it allows to avoid a pile up of output files that wouldconsume too much disk space
    elif int(dra) > maxruns:
        #defining the bacthes of simulations to run
        runrange = range(maxruns,int(dra),maxruns)
        if int(dra) != runrange[-1]:
            runrange.append(int(dra))
        for i in xrange(len(runrange)):
            #running fastsimcoal2 simulations
            cmdA = './fsc252 -t %s.tpl -n %s -e %s.est -E 1 -q -s %s -c %s' %(pre, maxruns, pre, Nsnps, cores)
            job = subprocess.call(shlex.split(cmdA))
            
            #stopping simulations if fastsimcoal2 fails
            if job != 0:
                sys.exit('\nERROR: fastisimcoal run with error, pipeline stopped!\n')
            
            #processing fastsimcoal2 output files
            for F in os.listdir(outdir):
                pathF = '%s/%s' %(outdir, F)
                
                #deleting unnecessary files
                if F.endswith('.par') or F.endswith('.arb') or F.endswith('.simparam'):
                    os.remove(pathF)
                
                #converting output arlequin (.arp) files into .snp files
                elif F.endswith('.arp'):
                    cmdB = 'python ./extract_SNPs_arlfile.py -d %s' %(outdir)
                    job = subprocess.call(shlex.split(cmdB))
                    
                    #removing .arp file after being processed
                    os.remove(pathF)
                    
                    #adding suffix to simulation name to distinguish which batch this run corresponds to
                    newend = '-B%d.snp' %i
                    os.rename(pathF.replace('.arp','.snp'), pathF.replace('.arp','.snp').replace('.snp', newend))

#generating output files for origin estimation
for F in os.listdir(outdir):
    pathF = '%s/%s' %(outdir, F)
    if F.endswith('.snp'):

        #converting snp file into snapp file for running Psi (if requested)
        if doDirIndex:
            cmdE = 'python snp_to_snapp.py -f %s' %pathF
            job = subprocess.call(shlex.split(cmdE))
         
        #converting snp file into a matrix to transpose it to fit dadi's necessary input format
        n = 0
        with open(pathF, 'r') as infile:
            for line in infile:
                n += 1
                row = line.strip().split()
                if n == 1:
                    SNPmat = np.empty([1,len(row)])
                row[0] = row[0].split('_')[0]
                SNPmat = np.vstack((SNPmat, row))
        SNPmat = np.delete(SNPmat,0,0)
        SNPmat = np.transpose(SNPmat)
        popnames = SNPmat[0,0::2]
        
        #generating input file for dadi
        outfilename = './%s/%s.dadi' %(outdir, F.split('.')[0])
        outfile = open(outfilename, 'w')
        devfreq = {}
        for r in xrange(np.shape(SNPmat)[0]):
            for c in xrange(1,np.shape(SNPmat)[1],2):
                if r == 0:
                    devfreq.setdefault(SNPmat[0,c],None)
                else:
                    devfreq[SNPmat[0,c]] = sum([int(x) for x in SNPmat[r,c-1:c+1]])
            if r == 0:
                print >> outfile, 'InOne\tInTwo\tAllele1\t%s\tAllele2\t%s\tGene\tPosition' %('\t'.join(['P'+x for x in popnames]), '\t'.join(['P'+x for x in popnames]))
            else:
                der = []
                for popname in popnames:
                    der.append(devfreq.get(popname))
                vector2 = np.repeat([2],len(der)).tolist()
                
                #assuring the minor allele is considered the "derived" allele for polarization
                if sum(der) > (np.shape(SNPmat)[1]/2):
                    der = [vector2[x] - der[x] for x in xrange(len(der))]
                ref = [vector2[x] - der[x] for x in xrange(len(der))]
                print >> outfile, '-A-\t-T-\tA\t%s\tT\t%s\tSNP%d\tPos%d' %('\t'.join([str(x) for x in der]), '\t'.join([str(x) for x in ref]), r, r)
        outfile.close()
        
        #removing temporal .snp file after being processed
        os.remove(pathF)

#estimating multiSFS with dadi
for F in os.listdir(outdir):
    pathF = '%s/%s' %(outdir, F)
    if F.endswith('.dadi'):
        #generating multiSFS file
        cmdC = 'python dadi_SFS_creator.py -F %s --multi True --polar True' %(pathF)
        job = subprocess.call(shlex.split(cmdC))
        os.remove(pathF)
        ending = F.replace(pre,'').replace('.dadi','')
        
        #modifying dadi's output file to remove the unneeded bottom line
        cmdD = 'head -n 2 ./multiSFS.sfs > %s/multiSFS%s.sfs' %(outdir, ending)
        job = subprocess.call(cmdD, shell=True)
        os.remove('./multiSFS.sfs')
        
#moving paramfile inside output folder
newname = '%s/%s.params' %(outdir, pre)
os.rename(paramfilename, newname)

#printing out message to let users know simulations finished
endmessage = " SIMULATION %s FINISHED " %pre
print        
print '{:#^60s}'.format(endmessage)
print
