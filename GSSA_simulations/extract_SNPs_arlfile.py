#!/usr/bin/env python
'''
script to read arlequin input files and extract the SNPs or DNA sequences. Note
this script requires sequences in input file listed individually instead of
grouped by haplotype.
Usage: python ./arlfile_SNPs_extract2.py -d [output directory] -T [data type]
Diego F. Alvarado-S. Aug 24, 2014
'''

import os, re, sys, operator, numpy as np
from collections import Counter
from optparse import OptionParser
usage = "usage: %prog -d [-T]"
description = "Purpose: to extract SNP or DNA data from an arlequin input file"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-d",dest="workdir", help="path where the output should be stored")
parser.add_option("-T",dest="Type", default='DNA', help="whether the input arlfile contains data in DNA or SNP format [DNA*/SNP]")
(options,args) = parser.parse_args()

path = options.workdir
datatype = options.Type

for filename in os.listdir(path):
    if '.arp' in filename:
        #creating output file
        outfilename = '%s/%s.snp' %(path, filename.split('.')[0])
        outfile = open(outfilename, 'w')
        infilename = '%s/%s' %(path, filename)
        pattern = re.compile('[0-9]*_[0-9]*')
        
        #reading in input file and extracting the information
        with open(infilename, 'r') as infile:
            n = 0
            indnames = []
            for line in infile:
                if pattern.match(line):
                    n += 1
                    row = line.strip().split('\t')
                    
                    #verifying that input file have sequences listed individually
                    if row[1] != '1':
                        print 'ERROR: frequency greatert than 1 -> sequences are not listed individually as required'
                    
                    #extracting data according to its type
                    if datatype == 'SNP':
                        print >> outfile, row[0] + '\t'.join(list(row[2]))  #note row[1] is removed because it indicates the frequency of that haplotype in the arlequin file and is not part of the sequence
                    elif datatype == 'DNA':
                        indnames.append(row[0])
                        datarow = [int(x) for x in list(row[2])]
                        if n == 1:
                            Data = np.asarray(datarow, dtype=int)
                        else:
                            Data = np.vstack((Data, datarow))
                    else:
                        sys.exit('ERROR: Incorrect data type defined')
        if datatype == 'DNA':
            keep = []
            for c in xrange(Data.shape[1]):
                alleles = Counter(Data[:,c])
                if len(alleles.keys()) == 2:
                    mostfreq = sorted(alleles.iteritems(), key=operator.itemgetter(1), reverse=True)[0]
                    keep.append(mostfreq[0])
                else:
                    keep.append('N')
            for r in xrange(Data.shape[0]):
                snpline = Data[r,:]
                newsnpline = []
                for x in xrange(len(snpline)):
                    if keep[x] != 'N':
                        if snpline[x] == keep[x]:
                            newsnpline.append('0')
                        else:
                            newsnpline.append('1')
                    else:
                        pass
                print >> outfile, indnames[r] + '\t'.join(newsnpline[:5001])
        outfile.close()
