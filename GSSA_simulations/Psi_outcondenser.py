#!/usr/bin/env python

'''
simple script to condense output files from multiple directionality index calculations on simulated
data. Note that this script is specific for the GSSA simulation pipeline implemented in
Alvarado-Serrano and Hickerson (in review); it would have to be customized for other projects.
Usage: python Psi_outcondenser.py -d [output directory]
Diego F. Alvarado, Jan-20-2018
'''

import os
from optparse import OptionParser

usage = "Usage: %prog"
description = "Purpose: to condense output files from multiple spatial-SFS calculations on simulated data"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-d", dest="OutDir", default='./', help="path to output folder where Psi output files are stored")
(options,args) = parser.parse_args()

outdir = options.OutDir
if not outdir.endswith('/'):
	outdir = outdir + '/'

#creating file to store results
Psiout = '%sPsi_index_combined.tsv' %outdir

#reading in individual Psi resu;ts and combining them into a single file
m = 0
with open(Psiout, 'w') as Psifile:
	for F in os.listdir(outdir):
		if F.endswith('_Psi_results.tsv'):
			m += 1
			
			#determining run prefix to label results
			parcomb = F.split('_')[2]
			param = parcomb.split('-')[0][:3]
			parval = parcomb.split('-')[0][3:]
			source = parcomb.split('-')[1]
			
			#reading individual files and storing results
			File = '%s%s' %(outdir, F)
			with open(File, 'r') as infile:
				n = 0
				for line in infile:
					n += 1
					row = line.strip()
					if m == 1:
						if n == 1:
							print >> Psifile, 'Parameter\tValue\tSource\t%s' %row
					else:
						if n > 1:
							print >> Psifile, '%s\t%s\t%s\t%s' %(param, parval, source, row)
			os.remove(File)
