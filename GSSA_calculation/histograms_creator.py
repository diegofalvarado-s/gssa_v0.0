#!/usr/bin/env python

'''
script to estimate the geographic-distance (hgeo) and geo-genetic (hgen) histograms from *folded*
multiSFS output files generated with dadi (with the bottom line removed). Note that this script
assumes that the multiSFS was created based on polarization on the minor allele.
Usage: python GSSA_creator.py -f [input folder] -g [samples coordinates or effective distances]
Diego F. Alvarado-S., April 22, 2015
MODIFIED: Nov 27, 2015
'''

import os, sys, subprocess, warnings, itertools as it, numpy as np
from collections import Counter
from scipy import spatial
from optparse import OptionParser

usage = "usage: %prog [options] arg"
description = "Purpose: to estimate the geographic-distance and geo-genetic histograms stat from a *folded* multiSFS file"
parser = OptionParser(usage=usage, description=description)
parser.add_option("-f", dest="File", help="path to the multiSFS file")
parser.add_option("-g", dest="geography", help="path to localities' coordinates or effective distances file")
(options,args) = parser.parse_args()

infilename = options.File
geoname = options.geography

if '/' in infilename:
    outdir = infilename.split('/')
    outdir = '/'.join(outdir[:-1])
    infile = infilename.split('/')[-1]
else:
    outdir = '.'
    infile = infilename
prefix = infile.replace('multiSFS_','').replace('.sfs','')
outfilename = '%s/%s.histo' %(outdir, prefix)
print outfilename

if geoname.endswith('coord'):
    #reading in location coordinates and calculating geographic distance among them
    coords = np.empty([1,3])
    nloc = -1       #stores number of localities sampled
    with open(geoname, 'r') as coordfile:
        for line in coordfile:
            nloc += 1
            if nloc > 0:
                row = line.strip().split()
                row[0] = row[0].replace('P','')
                coords = np.vstack((coords, [float(x) for x in row]))
    coords = np.delete(coords,0,0)
    EuD = spatial.distance.pdist(coords[:,1:],'euclidean')
    AllEuD = list(EuD)
    popnames = coords[:,0]
    
elif geoname.endswith('.dist'):
    with open(geoname, 'r') as distfile:
        n = 0
        for line in distfile:
            n += 1
            if n == 1:
                popnames = [float(x.replace('P','')) for x in line.strip().split(',')]
                nloc = len(popnames)
            else:
                break
    distmat = np.genfromtxt(geoname, delimiter=',')
    distmat = np.delete(distmat,0,0)
    AllEuD = []
    n = -1
    for r in xrange(np.shape(distmat)[0]):
        n += 1
        for c in xrange(np.shape(distmat)[1]):
            if c > n and r!=c:
                AllEuD.append(distmat[r,c])
    EuD = np.array(AllEuD)

else:
    sys.exist('Geography file should be either of type ".coord" or ".dist"')

#creating the multiSFS frequency bins according to the number of sampled individuals (NOTE: currently this assumes only 1 diploid individual per locality, and biallelic SNPs. The script could be easily be expanded to relax the first assumption)
combs = []
for comb in it.product(range(3), repeat = nloc):
    combs.append(comb)

#labeling distance pairs
comb = []
for i in it.combinations(popnames, 2):
    comb.append(str(int(i[0])) + '/' + str(int(i[1])))
EuD = np.vstack((comb,EuD))

#establishing the binning scheme for both the geographic-distance and geo-genetic histograms
from Sturges_bins import sturges
bins = sturges(AllEuD)
bins.insert(0, 0.0)
bins.insert(0, -1.0)    #adding -1 here creates a bin to temporally store the singleton SNPs, which are not used for the GSSA creation because those are more rpone to be confounded by errors


#creating a null histogram expectation exclusively based on samples' relative position to each other (i.e., geographic-distance histogram)
geobins = {}
for l in list(popnames):
    loc = str(int(l))
    dists = [0]     #to include the distance of each popultion with itself, which is zero
    for x in xrange(len(comb)):
        if comb[x].startswith(loc+'/') or comb[x].endswith('/'+loc):
            dists.append(AllEuD[x])
    geo_hist = list(np.histogram(dists, bins)[0])
    geobins.setdefault(loc, geo_hist[1:])

  
#reading multiSFS file in and generating the geo-genetic histogram (called genHist below):
with open(outfilename, 'w') as outfile:
    #print header to output file storing geographic-distance and geo-genetic histograms
    print >> outfile, 'Pop\t%s\t%s' %('\t'.join(['geoB'+str(x) for x in range(1,(len(bins)-1))]), '\t'.join(['genB'+str(x) for x in range((len(bins)-1))]))
    
    #read in multiSFS file
    pathF = infilename
    n = 0
    with open(pathF, 'r') as sfsfile:
        genHist = {}
        for line in sfsfile:
            n += 1
            
            #setting a dictionary for each locality to store the aggregated relative spatial distribution of minor alleles (named as vector "Si" in the publication)
            if n == 1:
                pops = [str(int(x.replace('"P','').replace('"',''))) for x in line.strip().split()[-nloc:]]
                for p in pops:
                    genHist.setdefault(p,[])
                if set(pops) != set([str(int(x)) for x in popnames]):
                    os.remove(outfilename)
                    sys.exit('Mismatched sampled localities and coordinates!')
    
            #estimating the aggregated relative spatial distribution of minor alleles for each individual (named as vector "Si" in the publication)
            if n == 2:
                sfs = [float(x) for x in line.strip().split()]
                for xbin in xrange(len(sfs)):
                    if sfs[xbin] > 0.0:
                        #the next two lines check for splitted bins (i.e., non-integer) that are present in unfolded SFS
                        if not float(sfs[xbin]).is_integer():
                            os.remove(outfilename)
                            sys.exit('Unfolded SFS provided, folded required!')
                        #the next two lines check whether the combined frequency of each SNP is smaller than that of the alternative allele, which should be the case when using the minor allele for polarization
                        if sum(list(combs[xbin])) > nloc:
                            warnings.warn('Input SFS is not polarized based on minor allele')
                        
                        #storing the distance between copies of the minor alles for each SNP locus and individual
                        for j in xrange(len(combs[xbin])):
                            if combs[xbin][j] != 0:
                                pop = pops[j]
                                popsIN = [x for x in EuD[0,] if x.startswith(pop+'/') or x.endswith('/'+pop)]
                                popsTO = [x.replace(pop+'/','').replace('/'+pop,'') for x in popsIN]
                                indexes = [pops.index(x) for x in popsTO]
                                indexPop = pops.index(pop)
                                distpairs = []
                                count = Counter(list(combs[xbin]))
                                if count.get(0)==(nloc-1):
                                    for freq in xrange(int(sfs[xbin])):
                                        distpairs.append(-1.0)  #special value for private SNPs at frequency of 1. Because such allele may be an error, it is not actually consider in the ragedness index calculation, but here such instances are recorded just in case
                                else:
                                    if combs[xbin][j] == 2:
                                        for freq in xrange(int(sfs[xbin])):
                                            distpairs.append(0.0)
                                    TOcombs = np.array([pops, list(combs[xbin])])
                                    TOcombs = np.delete(TOcombs,indexPop,1)
                                    for k in xrange(len(popsIN)):
                                        ToPop = popsIN[k].split('/')
                                        del ToPop[ToPop.index(pop)]
                                        indexToPop = list(TOcombs[0,]).index(ToPop[0])
                                        if TOcombs[1,indexToPop] != '0':
                                            dist = float(EuD[1,list(EuD[0,]).index(popsIN[k])])
                                            times = int(TOcombs[1,indexToPop]) * combs[xbin][j]
                                            for t in xrange(times):
                                                for freq in xrange(int(sfs[xbin])):
                                                    distpairs.append(dist)
                                genHist[pop].extend(distpairs)
    
    #writing out geographic-distance and geo-genetic histograms to file
    for loc in [str(int(x)) for x in popnames]:
        print >> outfile, 'P%s\t%s\t%s' %(loc, '\t'.join([str(x) for x in geobins.get(loc)]), '\t'.join([str(y) for y in list(np.histogram(genHist.get(loc), bins)[0])]))
