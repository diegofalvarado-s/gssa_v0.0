#!/bin/sh

#wrapper script to estimate the geographic spectrum of shared alleles (GSSA) summary statistic
#vector and use it to estimate the spatial dynamics of a potential range expansion
#Usage: ./GSSA_calculation.sh [vcf input file] [sampled localities coordinates file] [whether to plot GSSA vectors]
#Diego F. Alvarado-S., Jan 26, 2018

FILE=$1
COORDS=$2
PLOT=$3

if [ "$1" = "-h" ]
then
	echo
	echo 'Usage: ./GSSA_calculation.sh (1) (2) (3)'
    echo 'Purpose: calculate GSSA statistic and estimate range expansion spatial dynamics using it'
    echo 'Arguments:'
    echo '\t(1): multiSFS input file'
    echo '\t(2): sampled localities coordinates or effective distance input file'
	echo '\t(3): whether to plot the estimated GSSA vectors [TRUE/FALSE]'
	echo
else
	#calculating multiSFS file
	multisfs="$(python multiSFS_creator.py -f $1)"
	
	#calculate geographic-distance and geo-genetic histograms as described in Alvarado-Serrano and Hickerson in review
	output="$(python histograms_creator.py -f $multisfs -g $2)"
	
	#identify spatial dynamics of possible range expansion
	Rscript origin_finder.R $output $2 $3
fi
