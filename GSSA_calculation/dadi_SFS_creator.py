#!/usr/bin/env python

'''
script to generate SFS and jointSFS plots using dadi and a .dadi file (which could be generated with
dadi_input_generator.py script)
Usage: python dadi_SFS_creator.py -F 100Pop_SeqExp2D_1_1.dadi --multi True --polar False --doPlot True --fsc False
Diego F. Alvarado-S., Nov 13, 2014
'''

import os, copy, dadi, numpy as np
from optparse import OptionParser
from itertools import combinations
from matplotlib.backends.backend_pdf import PdfPages

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-F",dest="Filename", help="name of the sNMF input file being converted")
parser.add_option("--multi", dest="multiSFS", default=False, help="whether multiSFS should be created")
parser.add_option("--polar", dest="polarize", default=False, help="whether folded or unfolded SFS should be created")
parser.add_option("--plot", dest="Plot", default=False, help="whether to plot the resultant SFS")
parser.add_option("--fsc", dest="fscformat", default=True, help="whether dadi output should be converted into fastsimcoal format")
(options,args) = parser.parse_args()

infilename = options.Filename
prefix = infilename.split('/')[-1].split('.')[0]
doMulti = options.multiSFS
doPolar = options.polarize
doPlot = options.Plot
if doPlot == 'False':
    doPlot = False
doFsc = options.fscformat
if doFsc == 'False':
    doFsc = False

n = 0
#Determining populations and their sample sizes
with open(infilename, 'r') as infile:
    sampleN = {}
    for line in infile:
        n += 1
        if n == 1:
            header = line.strip().split()
            All1Pos = header.index('Allele1')
            All2Pos = header.index('Allele2')
            popnames = header[(All1Pos+1):All2Pos]
            sampleNtemp = {}
            for pop in popnames:
                sampleNtemp.setdefault(pop,[])
                sampleN.setdefault(pop,[])
            Counts1head = header[(All1Pos+1):All2Pos]
            Counts2head = header[(All2Pos+1):(All2Pos + len(popnames) + 1)]
        else:
            row = line.strip().split()
            Counts1 = row[(All1Pos+1):All2Pos]
            Counts2 = row[(All2Pos+1):(All2Pos + len(popnames) + 1)]
            for pop in popnames:
                sampleNtemp[pop].append(int(Counts1[Counts1head.index(pop)]))
                sampleNtemp[pop].append(int(Counts2[Counts2head.index(pop)]))
            for pop in popnames:
                sampleN[pop].append(sum(sampleNtemp.get(pop)))
                sampleNtemp[pop] = []    #this is just to have the original dictionary reset

### GENERATE SFS with dadi from output created above
dadiIN = dadi.Misc.make_data_dict(infilename)
dtype = [('locname', 'S10'), ('Nsamples', int), ('order', int)]
values = []

for loc in popnames:
    values.append((loc, min(sampleN.get(loc)), 0))
locOrder = np.array(values, dtype=dtype)       # creates a structured array with all the info needed included minimum sample size for each loc
if np.std(locOrder['Nsamples']) > 0.0:
    locOrder = np.sort(locOrder, order=['Nsamples', 'locname'])

if len(sampleN.keys())>1:
    for i in xrange(len(locOrder)):
        locOrder[i][2] = len(locOrder) - i
    locPairs = list(combinations(list(locOrder[['locname'][0]]),2))

    if doPlot:
        print 'doPlot set to TRUE'
        pdfName = '%s_SFSs_biplot.pdf' %prefix
        plotOut = PdfPages(pdfName)
        from matplotlib import pyplot as plt
        for pair in locPairs:
            pairLoop = list(pair)
            dataP1 = list(locOrder[locOrder['locname']==pairLoop[0]][0])
            dataP2 = list(locOrder[locOrder['locname']==pairLoop[1]][0])
            pairN = [dataP1[1], dataP2[1]]
            name = '%s_jointDAF_%s_pop%d_%d.sfs' %(prefix, '-'.join(pairLoop), dataP1[2], dataP2[2])
            SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = pairLoop, projections = pairN , polarized = False)     #returns the *folded* spectrum
            if not doMulti:
                SFS.to_file(name)
            SFSplot = dadi.Plotting.plot_single_2d_sfs(SFS , vmin = 1)
            plotOut.savefig(SFSplot)
            plt.close()     #this was needed to prevent the number of legend colorbar to be continuosly added in ech loop
        plotOut.close()
else:
    print '\n\t\tNumber of populations = 1, no joint SFS produced\n'
    name = '%s_singleAF.sfs' %(prefix)
    SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = [locOrder[0][0]], projections = [locOrder[0][1]] , polarized = doPolar)
    SFS.to_file(name)    
 
    
#Generate multiSFS if correspondin option is chosen and there is less than 4 populations (because of memory issues)
if doMulti:
    if len(locOrder['locname']) >= 4 and doFsc:
        print '\tWARNING:\n\tmultiSFS generated, but number of populations exceed memory limit for using multiSFS in fastsimcoal\n'
    SFS = dadi.Spectrum.from_data_dict(dadiIN, pop_ids = locOrder['locname'], projections = locOrder['Nsamples'] , polarized = doPolar)
    SFS.to_file('multiSFS.sfs')        

### Switch the format from the SFSs created above with dadi to fastsimcoal format
if doFsc and not doPolar:
    for F in os.listdir(os.getcwd()):
        if not doMulti and 'jointDAF' in F:
            cd = 'python multiTOmatrixSFS.py -i %s -o %s' %(F, prefix)
            os.system(cd)
            os.remove(F)
        elif doMulti and 'multiSFS' in F:
            multiOutname = '%s_DSFS.obs' %prefix
            with open(multiOutname, 'w') as multiOutfile:
                print >> multiOutfile, '1 observations. No. of demes and sample sizes are on next line'
                print >> multiOutfile, '%d\t%s' %(len(locOrder['locname']), '\t'.join([str(x) for x in locOrder['Nsamples']]))
                N = 0
                with open(F, 'r') as multiInfile:
                    for line in multiInfile:
                        N += 1
                        if N > 1:
                            print >> multiOutfile, line.strip()
            os.remove(F)
        elif 'singleAF.sfs' in F:
            singleSFSCORname = '%s_SFS.obs' %prefix
            line1 = []
            for X in xrange(locOrder[0][1]):
                line1.append('d0_'+str(X))
            with open(singleSFSCORname, 'w') as singleSFSCOR:
                print >> singleSFSCOR, '1 observations'
                print >> singleSFSCOR, '\t'.join(line1)
                N = 0
                with open(F, 'r') as singleInfile:
                    for line in singleInfile:
                        N += 1
                        if N > 1:
                            print >> singleSFSCOR, line.strip()
            os.remove(F)   

