#!/usr/bin/env python
 
'''
script to convert a vcf file into a dadi input file. The script only retains bi-allelic SNPs and allows to filter SNPs for a minimum genotype quality. Note the script also assumes the name of the population to which individuals pertain is a prefix to their name in the vcf file (this could be changed in line 30 for different datasets)
Usage: python vcf_to_dadi.py -f [input vcf file] -o [output] --GQ [minimum genotype quality]
Diego F. Alvarado-S., Dec 19, 2014
'''
 
import os, copy, numpy as np
from optparse import OptionParser
 
usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-f", dest="VCFname", help="name of the VCF input file being converted")
parser.add_option("--GQ", dest="GQual", default=0, help="minimum genotype quality tolerated")
(options,args) = parser.parse_args()
 
infilename = options.VCFname
path, inbase = os.path.split(infilename)
outfilename = '%s/%s.dadi' %(path, inbase.split('.')[0])
minqual = int(options.GQual)
missfilename = '%s/Summary_%s_vcf.txt' %(path, inbase.split('.')[0])
 
with open(infilename, 'r') as infile:
    for line in infile:
        if line.startswith('#'):
            if line.startswith('#CHROM'):
                row = line.strip().split()
                startcol = row.index('FORMAT')
                indnames = [y.split()[0] for y in [x.replace('.sorted.bam', '') for x in row[startcol+1:]]]
            else:
                pass
        else:
            break

locs = {}
for y in indnames:  
    locs.setdefault(y,{})

header = ['Gene', 'Pos', 'Ref', 'Alt']
for x in indnames:
    header.append(x)
geno = np.empty([1,len(header)])
geno = np.vstack((geno, header))
geno = np.delete(geno,0,0)
reverse = []
n = 0
with open(missfilename, 'w') as missfile:
    print >> missfile, 'Tag\tP0/0\tP0/1\tP1/1\tM0/0\tM0/1\tM1/1'
    with open(infilename, 'r') as infile:
        for line in infile:
            if line.startswith('#'):
                pass
            else:
                n += 1
                countP = {'00':0, '01':0, '11':0}    #stores the number of passing homozygous reference, heterozygous, and homozygous alternative genotypes
                countF = {'00':0, '01':0, '11':0}    #stores the number of filtered-out homozygous reference, heterozygous, and homozygous alternative genotypes
                row = line.strip().split()
                genorow = row[:2]
                genorow.append(row[3])
                genorow.append(row[4])
                if len(row[4])==1:    #this removes non-biallelic SNPs
                    dic = {'0':row[3],'1':row[4]}
                    gq_pos = row[startcol].split(':').index('GQ')
                    for IND in row[startcol+1:]:
                        indinfo = IND.split(':')
                        Gqual = int(indinfo[gq_pos])
                        if Gqual >= minqual:
                            gen = indinfo[0].split('/')
                            indgeno = '%s/%s' %(dic.get(gen[0]), dic.get(gen[1]))
                            if indinfo[0] == '0/0':
                                countP['00'] += 1
                            elif indinfo[0] == '0/1':
                                countP['01'] += 1
                            elif indinfo[0] == '1/1':
                                countP['11'] += 1
                        else:
                            if indinfo[0] == '0/0':
                                countF['00'] += 1
                            elif indinfo[0] == '0/1':
                                countF['01'] += 1
                            elif indinfo[0] == '1/1':
                                countF['11'] += 1
                            indgeno = '-/-'
                        genorow.append(indgeno)
                    if (countP.get('01') + countP.get('11')) > 0: #keeping only variable SNPs
                            if len(indnames) not in countP.values():
                                geno = np.vstack((geno, genorow))
                                if (countP.get('01') + (2*countP.get('11'))) > len(indnames):    #assuring the less frequent allele is chosen as minor allele
                                    reverse.append(True)
                                else:
                                    reverse.append(False)
                print >> missfile, '%s\t%d\t%d\t%d\t%d\t%d\t%d' %(row[0], countP.get('00'), countP.get('01'), countP.get('11'), countF.get('00'), countF.get('01'), countF.get('11'))

#print '\nRetained variable SNPs: %d' %(np.shape(geno)[0] - 1)

with open(outfilename,'w') as dadifile:
    print >> dadifile, 'InOne\tInTwo\tAllele1\t%s\tAllele2\t%s\tGene\tPosition' %('\t'.join(locs.keys()), '\t'.join(locs.keys()))
    for row in xrange(1,np.shape(geno)[0]):
        locsRow = copy.deepcopy(locs)
        if reverse[(row-1)]:
            alleRef = geno[row,3]
            alleDev = geno[row,2]
        else:
            alleRef = geno[row,2]
            alleDev = geno[row,3]
        for col in xrange(4,np.shape(geno)[1]):
            loc = geno[0,col]
            locsRow[loc].setdefault(alleRef,[])
            locsRow[loc].setdefault(alleDev,[])
            if geno[row,col] != '-/-':
                for a in geno[row,col].split('/'):
                    locsRow[loc][a].append(1)
        reffreq = []
        devfreq = []
        for l in locsRow.keys():
            reffreq.append(str(sum(locsRow[l].get(alleRef))))
            if alleDev != alleRef:
                devfreq.append(str(sum(locsRow[l].get(alleDev))))
        print >> dadifile, '-%s-\t-%s-\t%s\t%s\t%s\t%s\t%s\t%s' %(alleDev, alleRef, alleDev, '\t'.join(devfreq), alleRef, '\t'.join(reffreq), geno[row,0], geno[row,1])
