'''
script to estimate a multiSFS output file with dadi based on polarization on
the minor allele (with the bottom line removed) to serve as input for GSSA
creation. Note the script 
Usage: python multiSFS_creator.py -f [input vcf file]
Diego F. Alvarado-S., April 22, 2015
MODIFIED: Jan 14, 2016
'''

import os
from optparse import OptionParser

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-f",dest="File", help="name of vcf input file")
(options,args) = parser.parse_args()

infile = options.File
path, inbase = os.path.split(infile)
outfile = infile.split('.')[0] + '.geno'

cmd1 = 'python vcf_to_dadi.py -f %s' %infile
os.system(cmd1)

cmd2 = 'python dadi_SFS_creator.py -F %s/%s.dadi --multi True --polar True --plot False --fsc False' %(path, inbase.split('.')[0])
os.system(cmd2)

cmd3 = 'head -n 2 ./multiSFS.sfs > %s/multiSFS_%s.sfs && rm ./multiSFS.sfs' %(path, inbase.split('.')[0])
os.system(cmd3)

print '%s/multiSFS_%s.sfs' %(path, inbase.split('.')[0])
